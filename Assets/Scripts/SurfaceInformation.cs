﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceInformation : MonoBehaviour
{
    public Surfaces surface;
    public float UpwardThrust;
    public static Surfaces CurrentlySelectedSurface;
    public static int TotalTrampolineCount = 7;
    public GameObject NextSurface;
    public bool BreakAble;
    public int TrampolineNumber; 
}
