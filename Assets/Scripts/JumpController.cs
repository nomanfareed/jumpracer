﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    public PlayerController PC;
    public GameObject Model;
    public Animator ModelAnimator;
    public ParticleSystem LongJumpParticles;

    IEnumerator Jump(float Thrust)
    {
        if (Thrust == 0)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            UIController.instance.GameOver();
        }
        int random = Random.Range(1, 4);
        ModelAnimator.SetTrigger("Jump"+random.ToString());
        Model.GetComponent<Rigidbody>().velocity = new Vector3(0, Thrust, 0);
        yield return new WaitUntil(() => Model.GetComponent<Rigidbody>().velocity.y < 0);
        ModelAnimator.SetTrigger("Fall");
    }
    void OnAnimatorValueChange(float newValue)
    {
        Model.GetComponent<Rigidbody>().velocity = new Vector3(0, newValue, 0);
    }
    void ChangeOrientationTowardsNextTarget(Transform CurrentTransform , Transform NextTransform)
    {
        GameObject TempC = new GameObject("TempC");
        TempC.transform.position = CurrentTransform.position;
        TempC.transform.rotation = CurrentTransform.rotation;
        GameObject TempN = new GameObject("TempN");
        TempN.transform.position = NextTransform.position;
        TempN.transform.rotation = NextTransform.rotation;
        TempC.transform.LookAt(TempN.transform.position);
        float from = Model.transform.eulerAngles.y;
        float to = TempC.transform.eulerAngles.y;

        if (Mathf.Abs(from - to) > 180)
        {
            from = from + 360;
        }
        iTween.ValueTo(gameObject,iTween.Hash("from",from,"to",to,"time",0.3,"onupdate","OnValueChange","onupdatetarget",gameObject, "oncomplete", "OnValueAchieved", "oncompletetarget", gameObject));
        PC.RotationEnabled = false;

        Destroy(TempC);
        Destroy(TempN);
    }
    void OnValueChange(float newValue)
    {
        Model.transform.eulerAngles = new Vector3(0, newValue, 0);
    }
    void OnValueAchieved()
    {
        PC.RotationEnabled = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Checking if it has collided with something important
        if (!collision.collider.GetComponent<SurfaceInformation>())
        {
            return;
        }
        //Checking if it has reached final trampoline
        if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.FinalTrampoline)
        {
            UIController.instance.SettingPos("YOU");
            UIController.instance.GameCompleted();
            UIController.instance.UpdateBar(collision.collider.GetComponent<SurfaceInformation>().TrampolineNumber);
            ModelAnimator.SetTrigger("Dance");
            Model.GetComponent<Rigidbody>().isKinematic = true;
            Model.GetComponent<Collider>().enabled = false;
            return;
        }

        //Checking if it has hit water
        if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Water)
        {
            ModelAnimator.SetTrigger("Hit");
            UIController.instance.GameOver();
            return;
        }

        //Checking if it has hit any obstacle
        if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Obstacle)
        {
            collision.collider.enabled = false;
            ModelAnimator.SetTrigger("Dead");
            UIController.instance.GameOver();
            return;
        }
        //Break Trampoline if it is breakable
        if (collision.collider.GetComponent<SurfaceInformation>().BreakAble)
        {
            collision.collider.enabled = false;
            StartTrampolineBreakingAnimation(collision.collider.transform.GetChild(0).gameObject);
            UIController.instance.UpdateBar(collision.collider.GetComponent<SurfaceInformation>().TrampolineNumber);
        }
        else if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Bot)
        {
            collision.collider.enabled = false;
            Vector3 ExistingVelocity = collision.collider.GetComponent<Rigidbody>().velocity;
            Transform Bot = collision.collider.transform;
            Bot.GetComponent<Rigidbody>().velocity = new Vector3(Bot.forward.normalized.x * 5, ExistingVelocity.y, Bot.forward.normalized.z * 5);
            Bot.GetComponent<BotMovementController>().Dead(Model);
        }
        else if(collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Trampoline ||
            collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.MediumTrampoline ||
            collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.LargeTrampoline)
        {
            //Call for trampoline wave effect
            StartTrampolineWaveAnimation(collision.collider.transform.GetChild(0).gameObject);
            UIController.instance.UpdateBar(collision.collider.GetComponent<SurfaceInformation>().TrampolineNumber);
            //Call for character jump animation
            StartCoroutine(Jump(collision.collider.GetComponent<SurfaceInformation>().UpwardThrust));

            //Make character to look at the next trampoline if there is any
            if (collision.collider.GetComponent<SurfaceInformation>().NextSurface)
            {
                ChangeOrientationTowardsNextTarget(collision.collider.transform, collision.collider.GetComponent<SurfaceInformation>().NextSurface.transform);
            }
        }
        



    }
    void StartTrampolineWaveAnimation(GameObject TrampolineModel)
    {
        TrampolineModel.GetComponent<Animation>().Stop();
        TrampolineModel.GetComponent<Animation>().Play();
        if (TrampolineModel.transform.parent.GetComponentInChildren<SpriteRenderer>())
        {
            TrampolineModel.transform.parent.GetComponentInChildren<SpriteRenderer>().gameObject.GetComponent<Animation>().Play();
        }
    }
    void StartTrampolineBreakingAnimation(GameObject Trampoline)
    {
        Trampoline.GetComponent<Animation>().Play();
        //GetComponent<Rigidbody>().isKinematic = true;
        //while (Time.timeScale > 0.2f)
        //{
        //    Time.timeScale -= Time.deltaTime * 5;
        //    yield return new WaitForEndOfFrame();
        //}
        //while (Time.timeScale < 1)
        //{
        //    Time.timeScale += Time.deltaTime * 3;
        //    yield return new WaitForEndOfFrame();
        //}
        //GetComponent<Rigidbody>().isKinematic = false;
        //Call for character jump animation
        StartCoroutine(Jump(Trampoline.transform.parent.GetComponent<SurfaceInformation>().UpwardThrust));

        //Make character to look at the next trampoline if there is any
        if (Trampoline.transform.parent.GetComponent<SurfaceInformation>().NextSurface)
        {
            ChangeOrientationTowardsNextTarget(Trampoline.transform.parent, Trampoline.transform.parent.GetComponent<SurfaceInformation>().NextSurface.transform);
        }
    }
    void OnTimeScaleChange(float newVal)
    {
        Time.timeScale = newVal;
    }
}
