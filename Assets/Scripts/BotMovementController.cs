﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMovementController : MonoBehaviour
{
    public BotAIController BAIC;
    public GameObject Model;
    public Animator ModelAnimator;
    public ParticleSystem LongJumpParticles;

    //IEnumerator JumpAnimationHandler()
    //{
    //    int random = Random.Range(1, 3);
    //    ModelAnimator.SetTrigger("Jump"+random.ToString());
    //    yield return new WaitUntil(() => Model.GetComponent<Rigidbody>().velocity.y < 0);
    //    ModelAnimator.SetTrigger("Fall");
    //}
    //void OnAnimatorValueChange(float newValue)
    //{
    //    Model.GetComponent<Rigidbody>().velocity = new Vector3(0, newValue, 0);
    //}
    void ChangeOrientationTowardsNextTarget(Transform CurrentTransform , Transform NextTransform)
    {
        GameObject TempC = new GameObject("TempC");
        TempC.transform.position = CurrentTransform.position;
        TempC.transform.rotation = CurrentTransform.rotation;
        GameObject TempN = new GameObject("TempN");
        TempN.transform.position = NextTransform.position;
        TempN.transform.rotation = NextTransform.rotation;
        TempC.transform.LookAt(TempN.transform.position);
        float from = Model.transform.eulerAngles.y;
        float to = TempC.transform.eulerAngles.y;

        if (Mathf.Abs(from - to) > 180)
        {
            from = from + 360;
        }
        iTween.ValueTo(gameObject,iTween.Hash("from",from,"to",to,"time",0.3,"onupdate","OnValueChange","onupdatetarget",gameObject, "oncomplete", "OnValueAchieved", "oncompletetarget", gameObject, "oncompleteparams", CurrentTransform.gameObject));
        //PC.RotationEnabled = false;

        Destroy(TempC);
        Destroy(TempN);
    }
    void OnValueChange(float newValue)
    {
        Model.transform.eulerAngles = new Vector3(0, newValue, 0);
    }
    void OnValueAchieved(GameObject CurrentTrampoline)
    {
        //Debug.Log("hello hi by" + CurrentTrampoline.name);
        //BAIC.StartCoroutine(BAIC.MoveToNextTrampoline(CurrentTrampoline));
        //PC.RotationEnabled = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Checking if Bot has reached final trampoline
        if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.FinalTrampoline)
        {
            if (UIController.instance.GameIsCompleted)
            {
                UIController.instance.TriggerIfGameCompleted(name);
            }
            else
            {
                UIController.instance.SettingPos(name);
            }
            GetComponent<Rigidbody>().isKinematic = true;
            ModelAnimator.SetTrigger("Dance");
            return;
        }

        ////Checking if it has hit water
        //if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Water)
        //{
        //    ModelAnimator.SetTrigger("Hit");
        //    //UIController.instance.GameOver();
        //    return;
        //}

        ////Checking if it has hit any obstacle
        //if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Obstacle)
        //{
        //    GetComponent<Collider>().enabled = false;
        //    ModelAnimator.SetTrigger("Dead");
        //    //UIController.instance.GameOver();
        //    return;
        //}

        if (collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.Trampoline ||
            collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.MediumTrampoline ||
            collision.collider.GetComponent<SurfaceInformation>().surface == Surfaces.LargeTrampoline)
        {
            ChangeOrientationTowardsNextTarget(collision.collider.transform, BAIC.GetNextTrampoline(collision.collider.gameObject).transform);

            StartTrampolineWaveAnimation(collision.collider.transform.GetChild(0).gameObject);

            BotJumpController BJC = GetComponent<BotJumpController>();
            BJC.start = transform;
            BJC.target = BAIC.GetNextTrampoline(collision.collider.gameObject).transform;
            if(BJC.target.position.y - BJC.start.position.y > 0)
            {
                BJC.h = BJC.target.position.y - BJC.start.position.y + BJC.Default_h;
            }
            else
            {
                BJC.h = BJC.Default_h;
            }
            BJC.Launch();

            int random = Random.Range(1, 4);
            ModelAnimator.SetTrigger("Jump" + random.ToString());

            //UIController.instance.UpdateBar(collision.collider.GetComponent<SurfaceInformation>().TrampolineNumber);
            ////Call for character jump animation
            //StartCoroutine(Jump(collision.collider.GetComponent<SurfaceInformation>().UpwardThrust));

            ////Make character to look at the next trampoline if there is any
            //for (int i = 0; i < BAIC.Paths.Length; i++)
            //{
            //    if (GameObject.ReferenceEquals(collision.collider.gameObject, BAIC.Paths[i].gameObject))
            //    {

            //    }
            //}
        }
    }
    void StartTrampolineWaveAnimation(GameObject TrampolineModel)
    {
        TrampolineModel.GetComponent<Animation>().Stop();
        TrampolineModel.GetComponent<Animation>().Play();
        if (TrampolineModel.transform.parent.GetComponentInChildren<SpriteRenderer>())
        {
            TrampolineModel.transform.parent.GetComponentInChildren<SpriteRenderer>().gameObject.GetComponent<Animation>().Play();
        }
    }
    public void Dead(GameObject killer)
    {
        //transform.GetChild(0).parent = null;
        //GetComponent<BotRagdollController>().EnableRagdoll(killer);
        //GetComponent<Rigidbody>().isKinematic = false;
        //GetComponent<Rigidbody>().useGravity = false;
        ModelAnimator.SetTrigger("Dead");
        Destroy(Model, 2);
    }
}
