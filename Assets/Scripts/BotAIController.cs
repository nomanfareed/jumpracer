﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotAIController : MonoBehaviour
{
    public GameObject Model;
    public Transform[] Paths;

    public GameObject GetNextTrampoline(GameObject CurrentTrampoline)
    {
        for (int i = 0; i < Paths.Length; i++)
        {
            if (GameObject.ReferenceEquals(Paths[i].gameObject, CurrentTrampoline))
            {
                return Paths[i + 1].gameObject;
            }
        }
        return null;
    }

    //public IEnumerator MoveToNextTrampoline(GameObject CurrentTrampoline)
    //{
    //    GameObject NextTrampoline = null;
    //    for (int i = 0; i < Paths.Length; i++)
    //    {
    //        if (GameObject.ReferenceEquals(Paths[i].gameObject, CurrentTrampoline))
    //        {
    //            NextTrampoline = Paths[i+1].gameObject;
    //        }
    //    }
    //    Vector3 val1 = new Vector3(Model.transform.position.x, 0, Model.transform.position.z);
    //    Vector3 val2 = new Vector3(NextTrampoline.transform.position.x, 0, NextTrampoline.transform.position.z);
    //    while (Vector3.Distance(val1, val2) > 1)
    //    {
    //        float Old_Y = Model.transform.position.y;
    //        Model.transform.position += Model.transform.forward * Time.deltaTime * 12;
    //        Model.transform.position = new Vector3(Model.transform.position.x, Old_Y, Model.transform.position.z);
    //        yield return new WaitForEndOfFrame();
    //        val1 = new Vector3(Model.transform.position.x, 0, Model.transform.position.z);
    //        val2 = new Vector3(NextTrampoline.transform.position.x, 0, NextTrampoline.transform.position.z);
    //    }
    //}
}
