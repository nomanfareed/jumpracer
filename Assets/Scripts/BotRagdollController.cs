﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotRagdollController : MonoBehaviour
{
    public Collider[] RagdollColliders;

    // Start is called before the first frame update
    public void EnableRagdoll(GameObject Killer)
    {
        for(int i = 0; i < RagdollColliders.Length; i++)
        {
            RagdollColliders[i].enabled = true;
            //RagdollColliders[i].GetComponent<Rigidbody>().drag = 0.8f;
            RagdollColliders[i].GetComponent<Rigidbody>().velocity = Killer.transform.forward.normalized * 7;
            //RagdollColliders[i].GetComponent<Rigidbody>().velocity = K


        }
    }

    // Update is called once per frame
    void Start()
    {
        for (int i = 0; i < RagdollColliders.Length; i++)
        {
            RagdollColliders[i].enabled = false;
        }
    }
}
