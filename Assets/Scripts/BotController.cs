﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BotController : MonoBehaviour
{
    public Vector3 CurrentGroundPosition;
    public Transform GroundReflection;
    public Transform Spotlight;
    public Transform SpotlightLine;
    public Transform SpotlightGroundSpot;
    public JumpController JC;
    public Surfaces CurrentSurface;
    public bool MovementFrozen = false;
    [HideInInspector]
    public bool RotationEnabled = true;

    //Private Members
    bool invalidtouch = false;
    Vector3 LastTouchPosition;

    //Public Functions
    

    //Private Functions
    void Start()
    {
        Application.targetFrameRate = 60;
    }
    
    void Update()
    {
        if (MovementFrozen)
        {
            return;
        }
        UpdateRotation();
        UpdateForwardPosition();
        UpdateGroundPosition();
        HandleSpotLight();
    }

    void UpdateGroundPosition()
    {
        Ray ray = new Ray(JC.transform.position, -JC.transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            CurrentGroundPosition = hit.point;
            GroundReflection.position = CurrentGroundPosition;
            if(hit.collider.name == "Trampoline")
            {
                SurfaceInformation.CurrentlySelectedSurface = Surfaces.Trampoline;
            }
            else if (hit.collider.name == "MediumTrampoline")
            {
                SurfaceInformation.CurrentlySelectedSurface = Surfaces.MediumTrampoline;
            }
            else if(hit.collider.name == "LargeTrampoline")
            {
                SurfaceInformation.CurrentlySelectedSurface = Surfaces.LargeTrampoline;
            }
            else if (hit.collider.name == "FinalTrampoline")
            {
                SurfaceInformation.CurrentlySelectedSurface = Surfaces.FinalTrampoline;
            }
            else if (hit.collider.name == "Water")
            {
                SurfaceInformation.CurrentlySelectedSurface = Surfaces.Water;
            }
        }
    }

    void UpdateForwardPosition()
    {
        if (Input.GetMouseButton(0))
        {
            float Old_Y = JC.transform.position.y;
            JC.transform.position += JC.transform.forward * Time.deltaTime * 12;
            JC.transform.position = new Vector3(JC.transform.position.x, Old_Y, JC.transform.position.z);
            //UpdateGroundPosition();
        }
    }
    
    void UpdateRotation()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            LastTouchPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            float deltaX = Input.mousePosition.x - LastTouchPosition.x;
            JC.Model.transform.localEulerAngles = new Vector3(
                JC.Model.transform.localEulerAngles.x,
                JC.Model.transform.localEulerAngles.y + deltaX / 5,
                JC.Model.transform.localEulerAngles.z);
        }
        LastTouchPosition = Input.mousePosition;
#endif
#if UNITY_ANDROID
        if (!RotationEnabled)
        {
            return;
        }
        if(Input.touchCount != 1)
        {
            if(Input.touchCount == 0)
            {
                invalidtouch = false;
            }
            if(Input.touchCount > 1)
            {
                invalidtouch = true;
            }
            return;
        }
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            LastTouchPosition = Input.GetTouch(0).position;
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            float deltaX = Input.GetTouch(0).position.x - LastTouchPosition.x;
            JC.Model.transform.localEulerAngles = new Vector3(
                JC.Model.transform.localEulerAngles.x,
                JC.Model.transform.localEulerAngles.y + deltaX/5,
                JC.Model.transform.localEulerAngles.z);
        }
        LastTouchPosition = Input.GetTouch(0).position;
#endif
    }

    void HandleSpotLight()
    {
        float length = Vector3.Distance(GroundReflection.position, JC.Model.transform.position);
        Spotlight.localScale = new Vector3(1, length, 1);
        if(SurfaceInformation.CurrentlySelectedSurface == Surfaces.Trampoline)
        {
            SpotlightLine.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 0, 0.15f);
            SpotlightGroundSpot.GetComponent<SpriteRenderer>().material.color = new Color(0, 1, 0, 0.4f);
        }
        else if (SurfaceInformation.CurrentlySelectedSurface == Surfaces.MediumTrampoline)
        {
            SpotlightLine.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 0, 0.15f);
            SpotlightGroundSpot.GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 0, 0.4f);
        }
        else if (SurfaceInformation.CurrentlySelectedSurface == Surfaces.LargeTrampoline)
        {
            SpotlightLine.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 1, 0.15f);
            SpotlightGroundSpot.GetComponent<SpriteRenderer>().material.color = new Color(0, 0, 1, 0.4f);
        }
        else if(SurfaceInformation.CurrentlySelectedSurface == Surfaces.Water)
        {
            SpotlightLine.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, 0.15f);
            SpotlightGroundSpot.GetComponent<SpriteRenderer>().material.color = new Color(1, 0, 0, 0.4f);
        }
        else if (SurfaceInformation.CurrentlySelectedSurface == Surfaces.FinalTrampoline)
        {
            SpotlightLine.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, 0);
            SpotlightGroundSpot.GetComponent<SpriteRenderer>().material.color = new Color(0, 0, 0, 0);
        }
    }
}
