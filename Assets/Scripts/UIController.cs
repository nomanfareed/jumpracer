﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;
    public PlayerController PC;
    public ParticleSystem PS;
    public GameObject GameOverPanel;
    public GameObject GameCompletedPanel;
    public Image Bar;
    public bool GameIsCompleted = false;

    void Start()
    {
        instance = this;
    }
    public void GameOver()
    {
        PC.TurnOnSpotLight();
        PC.MovementFrozen = true;
        GameOverPanel.SetActive(true);
        GameObject Panel = GameOverPanel.transform.GetChild(0).gameObject;
        GameObject Title = GameOverPanel.transform.GetChild(0).GetChild(0).gameObject;
        GameObject Button = GameOverPanel.transform.GetChild(0).GetChild(1).gameObject;

        iTween.ValueTo(GameOverPanel, iTween.Hash("from", 0, "to", 0.6f, "time", 0.3f, "onupdate", "OnValChangeGameOver", "onupdatetarget", gameObject));

        iTween.ScaleTo(Panel, iTween.Hash("scale", new Vector3(0.01f, 1, 1), "time", 0.3f, "delay", 0.3f));
        iTween.ScaleTo(Panel, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.6f));

        iTween.ScaleTo(Title, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.9f, "easetype", iTween.EaseType.easeOutBack));
        iTween.ScaleTo(Button, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 1.2f, "easetype", iTween.EaseType.easeOutBack));
    }
    void OnValChangeGameOver(float newval)
    {
        GameOverPanel.GetComponent<Image>().color = new Color(0, 0, 0, newval);
    }

    public void GameCompleted()
    {
        PC.TurnOnSpotLight();
        PS.Play();
        PC.MovementFrozen = true;
        Invoke("ShowCompletionPanel", 1.2f);
    }
    void ShowCompletionPanel()
    {
        GameCompletedPanel.SetActive(true);
        GameObject Panel = GameCompletedPanel.transform.GetChild(0).gameObject;
        GameObject Title = GameCompletedPanel.transform.GetChild(0).GetChild(0).gameObject;
        GameObject Button = GameCompletedPanel.transform.GetChild(0).GetChild(1).gameObject;
        GameObject Pos1 = GameCompletedPanel.transform.GetChild(1).gameObject;
        GameObject Pos2 = GameCompletedPanel.transform.GetChild(2).gameObject;
        GameObject Pos3 = GameCompletedPanel.transform.GetChild(3).gameObject;
        iTween.ValueTo(GameCompletedPanel,iTween.Hash("from",0,"to",0.6f,"time",0.3f,"onupdate","OnValChangeGameCompletion","onupdatetarget",gameObject));

        if (Pos1.transform.GetChild(0).GetComponent<Text>().text != "")
        {
            iTween.ScaleTo(Pos1, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.3f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "OnShowResultOnce", "oncompletetarget", gameObject));
        }
        if (Pos2.transform.GetChild(0).GetComponent<Text>().text != "")
        {
            iTween.ScaleTo(Pos2, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.6f, "easetype", iTween.EaseType.easeOutBack));
        }
        if (Pos3.transform.GetChild(0).GetComponent<Text>().text != "")
        {
            iTween.ScaleTo(Pos3, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.9f, "easetype", iTween.EaseType.easeOutBack));
        }

        iTween.ScaleTo(Panel, iTween.Hash("scale", new Vector3(0.01f, 1, 1), "time", 0.3f, "delay", 1.2f));
        iTween.ScaleTo(Panel, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 1.5f));
        
        iTween.ScaleTo(Title, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 1.8f, "easetype",iTween.EaseType.easeOutBack));
        iTween.ScaleTo(Button, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 2.1f, "easetype", iTween.EaseType.easeOutBack));

    }
    void OnShowResultOnce()
    {
        GameIsCompleted = true;
    }
    void OnValChangeGameCompletion(float newval)
    {
        GameCompletedPanel.GetComponent<Image>().color = new Color(0, 0, 0, newval);
    }

    public void Restart()
    {
        Application.LoadLevel(0);
    }
    public void UpdateBar(int TrampolineNumber)
    {
        float val = TrampolineNumber * 1.0f / SurfaceInformation.TotalTrampolineCount;
        if (val > Bar.fillAmount)
        {
            iTween.ValueTo(gameObject, iTween.Hash("from", Bar.fillAmount, "to", val, "time", 0.3f, "onupdate", "OnBarChange"));
        }
    }
    void OnBarChange(float newVal)
    {
        Bar.fillAmount = newVal;
    }
    public void SettingPos(string name)
    {
        GameObject Pos1 = GameCompletedPanel.transform.GetChild(1).gameObject;
        GameObject Pos2 = GameCompletedPanel.transform.GetChild(2).gameObject;
        GameObject Pos3 = GameCompletedPanel.transform.GetChild(3).gameObject;
        if(Pos1.transform.GetChild(0).GetComponent<Text>().text == "")
        {
            Pos1.transform.GetChild(0).GetComponent<Text>().text = name;
        }
        else if (Pos2.transform.GetChild(0).GetComponent<Text>().text == "")
        {
            Pos2.transform.GetChild(0).GetComponent<Text>().text = name;
        }
        else
        {
            Pos3.transform.GetChild(0).GetComponent<Text>().text = name;
        }
    }
    public void TriggerIfGameCompleted(string name)
    {
        Debug.Log(name);
        if (GameCompletedPanel.activeSelf)
        {
            GameObject Pos2 = GameCompletedPanel.transform.GetChild(2).gameObject;
            GameObject Pos3 = GameCompletedPanel.transform.GetChild(3).gameObject;
            if (Pos2.transform.GetChild(0).GetComponent<Text>().text == "")
            {
                Pos2.transform.GetChild(0).GetComponent<Text>().text = name;
                iTween.ScaleTo(Pos2, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.6f, "easetype", iTween.EaseType.easeOutBack));
            }
            else if (Pos3.transform.GetChild(0).GetComponent<Text>().text == "")
            {
                Pos3.transform.GetChild(0).GetComponent<Text>().text = name;
                iTween.ScaleTo(Pos3, iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "delay", 0.6f, "easetype", iTween.EaseType.easeOutBack));
            }
        }
    }
}
