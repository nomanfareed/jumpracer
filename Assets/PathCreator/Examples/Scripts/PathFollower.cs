﻿using UnityEngine;

namespace PathCreation.Examples
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreator pathCreator;

        void Start() {
            if (pathCreator.path.isClosedLoop)
            {
                GetComponent<LineRenderer>().positionCount = pathCreator.path.NumPoints + 1;
            }
            else
            {
                GetComponent<LineRenderer>().positionCount = pathCreator.path.NumPoints;
            }
            for (int i = 0; i < pathCreator.path.NumPoints; i++)
            {
                GetComponent<LineRenderer>().SetPosition(i, pathCreator.path.GetPoint(i));
            }
            if (pathCreator.path.isClosedLoop)
            {
                GetComponent<LineRenderer>().SetPosition(pathCreator.path.NumPoints, pathCreator.path.GetPoint(0));
            }
        }
    }
}